{
  description = "top level flake for host configs and deployments";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    nixos-hardware.url = "github:NixOS/nixos-hardware/master";
    sops-nix.url = "github:Mic92/sops-nix";
    deploy-rs.url = "github:serokell/deploy-rs";

    # nixos-generators
    # nix build .#nixosConfigurations.<host>.config.formats.raw-efi
    nixos-generators = {
      url = "github:nix-community/nixos-generators";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    # haumea configurations
    haumea = {
      url = "github:nix-community/haumea/v0.2.2";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    shared = {
      url = "path:./shared";
      inputs.nixpkgs.follows = "nixpkgs";
      #inputs.flake-parts.follows = "flake-parts";
    };

    # locally defined packages and patches
    locPkgs.url = "path:./nixpkgs";
    locPkgs.inputs.nixpkgs.follows = "nixpkgs";

    # directus
    directus.url = "gitlab:lone4/directus-flake-parts";

  };

  outputs = { self
    , nixos-hardware
    , nixpkgs
    , sops-nix
    , deploy-rs
    , nixos-generators
    , haumea
    , shared
    , locPkgs
    , directus
    , ... }@attrs:
    let

      allOverlays = { config, pkgs, ... }: {
        nixpkgs.overlays = [
          locPkgs.overlays.localPackages
          locPkgs.overlays.python
          locPkgs.overlays.default
          (final: super: {
            makeModulesClosure = x:
              super.makeModulesClosure (x // { allowMissing = true; });
          })
        ];
      };

      lib = nixpkgs.lib;

      hosts = haumea.lib.load { src = ./hosts;
        loader = haumea.lib.loaders.verbatim;
      };

      mkHosts = lib.mapAttrs (host: value: lib.nixosSystem {
        specialArgs = attrs;
        modules = [
          allOverlays
          directus.nixosModules.default
          sops-nix.nixosModules.sops
          nixos-generators.nixosModules.all-formats
        ] ++ lib.mapAttrsToList (_: v: v) value;
      }) hosts;

      mkDeployments = lib.mapAttrs (host: value: {
          hostname = "nowhere";
          profiles.system = let
            sys = self.nixosConfigurations.${host}.config.nixpkgs.hostPlatform.system;
            in {
            path = deploy-rs.lib.${sys}.activate.nixos self.nixosConfigurations.${host};
          };
      }) hosts;

    in {
      nixosConfigurations = mkHosts;

      deploy = {
        user = "root";
        sshUser = "root";
        nodes = mkDeployments;
      };
  };
}
