{ shared, ... }:
{
  imports = [
    shared.hardware.pinebook.hardware
    shared.hardware.pinebook.network
    shared.profiles.baseline
    shared.profiles.nvim
    shared.profiles.disable-docs
    shared.profiles.passwordless-sudo
    shared.profiles.gnome
    shared.packagesets.basics-plus
    shared.packagesets.desktop-office
    shared.services.default
  ];
}
