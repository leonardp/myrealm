{ config, ... }:
{
  networking.hostName = "heineken";
  nixpkgs.hostPlatform = "aarch64-linux";
  time.timeZone = "Europe/Berlin";
}
