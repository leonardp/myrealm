{ config, ... }:
{
  users.users = {
    root = {
      openssh.authorizedKeys.keys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIP28wwhUzXDAyq/Y/ETHrgHBOOT0kCWD8yRzYStuZ95D admin@living-systems.dev"
"ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICHqsTwRmG43cwtoD6L1LvD57WAyvfDvgnBOq8CPvPY9 automaton@living-systems.dev"
      ];
    };
    leo = {
      isNormalUser = true;
      openssh.authorizedKeys.keys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIP28wwhUzXDAyq/Y/ETHrgHBOOT0kCWD8yRzYStuZ95D admin@living-systems.dev"
      ];
      extraGroups = [ "video" "audio" "usb" "disk" "scanner" "lp" "dialout" "nm-openvpn" "networkmanager" "uucp" "keys" "libvirtd" "kvm" "docker" "wheel" ];
      hashedPassword = "$y$j9T$m.4b8VBgm8AtNAVpss4xU1$diaccfiNEYupA86rHiKk8cejAQs9C7SBAEfZvHLE3QD";
    };
  };
}
