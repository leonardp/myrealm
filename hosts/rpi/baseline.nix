{ config, lib, shared, ... }:
{
  imports = [
    shared.hardware.rpi.default
    shared.hardware.rpi.fs
    shared.services.default
    shared.profiles.default
    shared.profiles.passwordless-sudo
    shared.profiles.disable-docs
  ];

  users.users = {
    root = {
      openssh.authorizedKeys.keys = lib.mkDefault [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIP28wwhUzXDAyq/Y/ETHrgHBOOT0kCWD8yRzYStuZ95D admin@living-systems.dev"
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICHqsTwRmG43cwtoD6L1LvD57WAyvfDvgnBOq8CPvPY9 automaton@living-systems.dev"
      ];
    };
  };
}
