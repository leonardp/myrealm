{ shared, ... }:
{
  imports = [
    shared.hardware.qemu.common
    shared.hardware.qemu.libvirt
    shared.profiles.baseline
    shared.profiles.nvim
    shared.profiles.disable-docs
    shared.profiles.passwordless-sudo
    shared.packagesets.basics-plus
    shared.services.default
  ];
}
