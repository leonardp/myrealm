{ config, shared, pkgs, ... }:
let
  dS = pkgs.writeText "ds" "7323e0c9-9a5d-4426-ac11-8e0426e6f889";
  dK = pkgs.writeText "dk" "d4be5f65-5a0f-43ce-ba48-fd83fd86f249";
in
{
  imports = [
    shared.modules.directos
  ];

  services.directos = {
    enable = true;
    directosSecretFile = dS;
    directosKeyFile = dK;
  };

  sops = {
    defaultSopsFile = ../../secrets/directus.yaml;
    age.sshKeyPaths = [ "/root/.ssh/id_ed25519" ];

    secrets = {
      "directos/key" = {
        key = "directos/key";
        mode = "0400";
        owner = "directus";
        group = "directus";
      };

      "directos/secret" = {
        key = "directos/secret";
        mode = "0400";
        owner = "directus";
        group = "directus";
      };

      automaton = {
        #sopsFile = ../../secrets/ssh.yaml;
        sopsFile = ../../secrets/ssh.yaml;
        key = "automaton";
        mode = "0400";
        owner = "directos";
        group = "directos";
      };
    };
  };

  users.users.directus.extraGroups = [ config.users.groups.keys.name ];
  systemd.services."directus-bootstrap-directos".after = [ "run-secrets.d.mount" ];

  users.users.directos.extraGroups = [ config.users.groups.keys.name ];
}
