{ config, ... }:
{
  networking.hostName = "nixos";
  nixpkgs.hostPlatform = "x86_64-linux";
  time.timeZone = "Europe/Berlin";
}
