{ config, ... }:
{
  users.users = {
    nixos = {
      initialPassword = "nixos";
      isNormalUser = true;
      extraGroups = [ "video" "audio" "usb" "disk" "scanner" "lp" "dialout" "nm-openvpn" "networkmanager" "uucp" "keys" "libvirtd" "kvm" "docker" "wheel" ];
    };
  };
}
