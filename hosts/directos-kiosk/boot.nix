{ config, lib, pkgs, ... }:
{
  # keymaps
  console = { keyMap = "de"; };
  services.xserver.xkb.layout = "de";
}
