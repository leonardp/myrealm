{ pkgs, lib, ... }:
{
  services.cage = {
    enable = true;
    user = "nixos";
    #program = "${pkgs.firefox}/bin/firefox --kiosk http://directos.local";
    program = "${pkgs.ungoogled-chromium}/bin/chromium --noerrdialogs --disable-infobars --kiosk http://directos.local http://directos.local";
    #environment = {
    #  #WLR_LIBINPUT_NO_DEVICES = "1";
    #  DRI_PRIME = "1";
    #};
    extraArguments =[
      "-d"
    ];
  };

  #programs.regreet = {
  #  enable = true;
  #  cageArgs = [ "-s" "-m" "last" ];
  #  settings = {
  #    default_session = {
  #      command = "${pkgs.xterm}/bin/xterm";
  #    };
  #    ff_session = {
  #      command = "${pkgs.firefox}/bin/firefox";
  #    };
  #  };
  #};
}
