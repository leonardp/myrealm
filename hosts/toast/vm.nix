{ config, ... }:
{
  virtualisation.vmVariant = {
    virtualisation = {
      memorySize =  2048;
      cores = 2;
      graphics = false;
    };
  };
}
