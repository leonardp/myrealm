{ config, ... }:
{
  networking.hostName = "toast";
  nixpkgs.hostPlatform = "x86_64-linux";
  time.timeZone = "Europe/Berlin";
}
