{ config, lib, ... }:
{

  networking.firewall.enable = false;

  networking = {
    dhcpcd.enable = false;

    defaultGateway = {
      address = "192.168.0.1";
    };

    nameservers = [ "1.1.1.1" "8.8.8.8" ];

    interfaces."enp1s0".ipv4 = {
      addresses = [{
        address = "192.168.0.20";
        prefixLength = 24;
      }];
      routes = [{
        address = "192.168.0.0";
        prefixLength = 24;
        via = "192.168.0.1";
      }];
    };
  };
}
