{ config, ... }:
{
  users.users = {
    root = {
      openssh.authorizedKeys.keys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIP28wwhUzXDAyq/Y/ETHrgHBOOT0kCWD8yRzYStuZ95D admin@living-systems.dev"
"ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICHqsTwRmG43cwtoD6L1LvD57WAyvfDvgnBOq8CPvPY9 automaton@living-systems.dev"
      ];
    };
  };
}
