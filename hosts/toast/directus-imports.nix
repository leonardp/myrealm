{ shared, ... }:
{
  imports = [
    shared.hardware.qemu.common
shared.hardware.qemu.libvirt
shared.profiles.default
shared.packagesets.basics-plus
shared.services.default
  ];
}
