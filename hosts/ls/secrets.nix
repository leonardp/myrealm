{ config, ... }:
{
  sops = {
    defaultSopsFile = ../../secrets/directus.yaml;
    age.sshKeyPaths = [ "/root/.ssh/id_ed25519" ];

    secrets = {
      "directus/key" = {
        key = "directus/key";
        mode = "0400";
        owner = "directus";
        group = "directus";
      };

      "directus/secret" = {
        key = "directus/secret";
        mode = "0400";
        owner = "directus";
        group = "directus";
      };

      hetzner = {
        sopsFile = ../../secrets/hetzner.yaml;
        key = "hetzner";
        mode = "0400";
        owner = "directus";
        group = "directus";
      };
    };
  };

  users.users.acme.extraGroups = [ config.users.groups.keys.name ];
  systemd.services."acme-living-systems.dev".after = [ "run-secrets.d.mount" ];

  users.users.directus.extraGroups = [ config.users.groups.keys.name ];
  systemd.services."directus-bootstrap-nixlos".after = [ "run-secrets.d.mount" ];
}
