{ config, lib, ... }:
{
  networking.hostName = "ls";

  networking.firewall.enable = true;
  #networking.firewall.checkReversePath = false;

  ## Open ports in the firewall.
  networking.firewall.allowedTCPPorts = [
    22    # ssh
    80    # http
    443   # https
  ];

  # shared
  #networking = {
  #  dhcpcd.enable = false;
  #  defaultGateway = {
  #    address = "172.31.1.1";
  #  };
  #  defaultGateway6 = {
  #    address = "fe80::1";
  #    #metric = 231;
  #    interface = "enp1s0";
  #  };
  #  interfaces = {
  #    "enp1s0" = {
  #      ipv4 = {
  #        addresses = [{
  #          "address" = "xxx.xxx.xxx.xxx";
  #          "prefixLength" = 32;
  #        }];
  #        routes = [{
  #            address = "172.31.1.1";
  #            prefixLength = 32;
  #            options = { scope = "link"; };
  #        }];
  #      };
  #    };
  #  };
  #};

  services.fail2ban = {
    enable = true;
    bantime-increment = {
      enable = true;
      overalljails = true;
      maxtime = "48h";
    };
    bantime = "42m";
  };
}
