{ config, lib, shared, ... }:
{
  imports = [
    shared.hardware.qemu.common
    shared.hardware.qemu.hetzner
    shared.services.default
    shared.profiles.default
  ];

  nixpkgs.hostPlatform = "x86_64-linux";

  users.users = {
    root = {
      openssh.authorizedKeys.keys = lib.mkDefault [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPc5dear8nQf3y1iUzTHibOlJDtdQjcxQXF28d8hyssF leo@private"
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIP28wwhUzXDAyq/Y/ETHrgHBOOT0kCWD8yRzYStuZ95D admin@living-systems.dev"
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICHqsTwRmG43cwtoD6L1LvD57WAyvfDvgnBOq8CPvPY9 automaton@living-systems.dev"
      ];
    };
  };
  #services.jitsi-meet = {
  #  enable = true;
  #  hostName = "meet.living-systems.dev";
  #};
}
