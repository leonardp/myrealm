{ config, pkgs, ... }:
let
  ls = "living-systems.dev";
  certDir = "/var/lib/acme/${ls}";

  vhostDefaults = {
    forceSSL = true;
    sslCertificate = "${certDir}/cert.pem";
    sslCertificateKey = "${certDir}/key.pem";
  };

in
{
  services.nginx = {
    clientMaxBodySize = "1g";
    enable = true;
    recommendedTlsSettings = true;
    recommendedGzipSettings = true;
    recommendedOptimisation = true;
    recommendedProxySettings = true;

    virtualHosts."${ls}" = vhostDefaults // {
      root = "/var/www/nixlos";
      serverAliases = [ "blog.${ls}" ];

      locations."/" = {
        index = "index.html";
      };
    };

    virtualHosts."directus.${ls}" = vhostDefaults // {
      locations."/" = {
        proxyPass = "http://127.0.0.1:8009";
        proxyWebsockets = true;
      };
    };
  };

  services.directus.instances = {

    nixlos = {
      enable = true;
      url = "https://directus.${ls}";
      address = "127.0.0.1";
      port = 8009;
      secretFile = config.sops.secrets."directus/secret".path;
      keyFile = config.sops.secrets."directus/key".path;
      env = {
        "DB_CLIENT" = "sqlite3";
        "DB_FILENAME" = "./data.db";
        "ADMIN_EMAIL" = "admin@example.com";
        "ADMIN_PASSWORD" = "admin";
        "MAX_PAYLOAD_SIZE" = "1gb";
        "CORS_ENABLED" = "true";
        "CORS_ORIGIN" = "https://${ls},https://nixlos.${ls}";
      };
      bootstrap = {
        schema = pkgs.nixlosSchema;
        migrations = pkgs.nixlosMigrations;
      };
    };
  };
}
