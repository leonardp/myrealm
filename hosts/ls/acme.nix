{ config, ... }:
{
  security.acme = {
    acceptTerms = true;
    defaults = {
      server = "https://acme-v02.api.letsencrypt.org/directory";
      email = "leonard.pollak@gmail.com";
      reloadServices = [
        "nginx"
      ];
    };
    #defaults = { server = "https://acme-staging-v02.api.letsencrypt.org/directory"; };

    certs = let ls = "living-systems.dev"; in {
      "${ls}" = {
        dnsProvider = "hetzner";
        credentialsFile = config.sops.secrets.hetzner.path;

        email = "admin@${ls}";
        extraDomainNames = [
          "*.${ls}"
        ];
        postRun = ''
          cp cert.pem key.pem /var/www/acme/${ls}/
          chown nginx:nginx /var/www/acme/${ls}/*
        '';
      };
    };
  };

  users.groups.nginx = {};

  users.users."nginx" = {
    isSystemUser = true;
    group = "nginx";
    extraGroups = [ "acme" ];
  };
}
