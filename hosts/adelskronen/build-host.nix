{ config, lib, ... }:
{
  # mount tmpfs on /tmp -> not good for build hosts
  boot.tmp.useTmpfs = lib.mkOverride 100 false;

  # emulate arm
  boot.binfmt.emulatedSystems = [ "aarch64-linux" ];

  # enable http api
  services.nix-serve = {
    enable = true;
    secretKeyFile = "/cache-priv-key.pem";
  };

  nix.settings.max-jobs = lib.mkDefault 16;

  # keep build-time dependencies
  nix.settings = {
    keep-outputs = true;
    keep-derivations = true;
    trusted-users =  [ "leo" ];
  };

  nix.settings = {
    "access-tokens" = ["github.com=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"];
  };

  nixpkgs.config.allowBroken = true;

  # nginx proxy
  services.nginx = {
    enable = true;
    virtualHosts = {
      "binarycache.local" = {
        locations."/".extraConfig = ''
          proxy_pass http://127.0.0.1:${toString config.services.nix-serve.port};
          proxy_set_header Host $host;
          proxy_set_header X-Real-IP $remote_addr;
          proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        '';
      };
    };
  };
}
