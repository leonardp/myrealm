{ config, pkgs, ... }:
{
  programs = {
    direnv.enable = true;
    steam.enable = true;
    alvr.enable = true;
    # nix-ld = {
    #   enable = true;
    #   libraries = [
    #     pkgs.libusb1
    #   ];
    # };
  };
  #hardware.steam-hardware.enable = true;

  #services.monado.enable = true;
  #services.monado.defaultRuntime = true;


  environment.systemPackages = with pkgs; [
    virtiofsd
    # davinci-resolve
    qbittorrent
    android-tools
    #woeusb-ng
    #ntfs3g
    meshroom
    darktable

    git-lfs

    # patool deps
    # bottles
  ];

  #virtualisation.libvirtd = {
  #  enable = true;
  #  # qemuPackage = pkgs.qemu_kvm;
  #};
  #virtualisation.spiceUSBRedirection.enable = true;

  # something strange...
  #security.apparmor.enable = false;

  virtualisation.docker.enable = true;
  hardware.nvidia-container-toolkit.enable = true;
}
