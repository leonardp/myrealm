{ config, lib, ... }:
{
  networking.useDHCP = false;
  #networking.useNetworkd = true; # experimental

  networking.enableIPv6 = false;

  boot.kernel.sysctl = {
    "net.ipv4.conf.all.forwarding" = true;
    #"net.ipv4.conf.all.arp_filter" = true;
    #"net.ipv6.conf.all.forwarding" = true;
  };


  # thank's steven
#  networking.extraHosts =
#    let
#      hostsPath = https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts;
#      hostsFile = builtins.fetchurl hostsPath;
#    in
#    builtins.readFile "${hostsFile}";
#
  #services.resolved.enable = lib.mkForce false;

  networking = {
    dhcpcd.enable = false;

    #search = [ "local" ];

    #defaultGateway = "192.168.0.1";
    nameservers = [ "1.1.1.1" "8.8.8.8" ];

    networkmanager.unmanaged = [
      "enp39s0"
    ];
    #macvlans.macvtap0 = {
    #  interface = "enp39s0";
    #  mode = "bridge";
    #};
    #interfaces.macvtap0 = {
    ##  macAddress = "52:54:00:f3:5b:12";
    ##interfaces.enp39s0 = {
    ##  mtu = 1500;
    ##  useDHCP = true;
    #  ipv4 = {
    #    addresses = [{
    #      address = "192.168.0.10";
    #      prefixLength = 24;
    #    }];
    #    routes = [{
    #      address = "192.168.0.0";
    #      prefixLength = 24;
    #      via = "192.168.0.1";
    #    }];
    #  };
    #};
  };

  networking.firewall.enable = false;
  #networking.firewall.checkReversePath = false;

  # Open ports in the firewall.
  networking.firewall.allowedTCPPorts = [
    22    # ssh
    80    # 80 nginx proxy for binary cache
    111   # SunRPC
    1716  # GSConnect
    2049  # NFS
    5432  # phppgadmin
    5555  # ggm-http
    8000  # http-shared
    49152 # zpinger
    34057 # zyre testapp
    20048 # zyre testapp
    42639 # zyre testapp
  ];

  networking.firewall.allowedUDPPorts = [
    123   # NTP
    111   # SunRPC
    1716  # GSConnect
    2049  # NFS
    5670  # ZRE-DISC
    43090 # zyre testapp
    59607 # zyre testapp
  ];

  # port range
  networking.firewall.allowedTCPPortRanges = [
    { from = 6000; to = 6010; }
    { from = 1714; to = 1764; }
  ];
}
