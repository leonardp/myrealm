{ shared, ... }:
{
  imports = [
    shared.profiles.default
shared.profiles.sound
shared.profiles.bluetooth
#shared.profiles.printing
shared.profiles.gnome
shared.profiles.passwordless-sudo
shared.packagesets.basics-plus
shared.packagesets.desktop-fat
shared.packagesets.dev
shared.packagesets.dev-python
shared.packagesets.dev-electronix
shared.services.default
shared.storage.zfs-tank
shared.storage.samba-server
  ];
}
