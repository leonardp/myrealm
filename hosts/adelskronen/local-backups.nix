{
  services.rsnapshot = {
    enable = false;
    cronIntervals = {
      #"hourly"  = "0 * * * *";
      "daily"   = "0 21 * * *";
      "monthly" = "0 0 1 * *";
    };
      #retain	hourly	24
    extraConfig = ''
      snapshot_root	/mnt/data/rsnapshots/

      retain	daily	7
      retain	monthly	6

      exclude	Downloads/
      exclude	__pycache__/
      exclude	.cache/
      exclude	OLD/
      exclude	torrent/
      exclude	tartube-data/
      exclude	linux/
      exclude	nix/
      exclude	.npm/

      backup	/home/	localhost/
      backup	/root/	localhost/
    '';
  };
}
