{ config, lib, pkgs, ... }:
{
  # keymaps
  console = { keyMap = "de"; };
  services.xserver.xkb.layout = "de";

  boot.loader.grub.enable = true;
  boot.loader.grub.memtest86.enable = true;
  boot.loader.timeout = 5;
  boot.loader.grub.device = "/dev/nvme0n1";

  #boot.kernelPackages = pkgs.linuxPackages_5_4;

  boot.initrd.availableKernelModules = [ "nvme" "ahci" "xhci_pci" "usbhid" ];
  boot.kernelModules = [ "kvm-amd" ];

  boot.kernelParams = [
    "acpi_enforce_resources=lax"
  ];

  boot.extraModprobeConfig = "options iwlwifi 11n_disable=1"; # whyyy?!?!

  hardware.enableRedistributableFirmware = true;
  hardware.enableAllFirmware = true;
  hardware.firmware = [ pkgs.firmwareLinuxNonfree ];

  # nonfree nvidia driver for Xorg and Wayland
  services.xserver.videoDrivers = ["nvidia"];

  hardware.nvidia = {
    modesetting.enable = true;
    powerManagement.enable = false;
    #powerManagement.finegrained = true;
    open = false;
    nvidiaSettings = true;
    #package = config.boot.kernelPackages.nvidiaPackages.latest;
  };

  # FIXME TODO
  # documnet this
  # extrapackages are needed for dearpygui to function
  hardware.graphics = {
    enable = true;
    enable32Bit = true;
    extraPackages = with pkgs; [
       libGL
       xorg.libX11
     ];
  };

  # default soundcard is not hdmi
  # FIXME sound option removed
  #sound.extraConfig = ''
  #  pcm.!default {
  #      type hw
  #      card 1
  #  }

  #  ctl.!default {
  #      type hw
  #      card 1
  #  }
  #'';

  fileSystems."/" = {
    #device = "/dev/disk/by-uuid/6133ea81-7374-4283-a4cf-66d222f451bb";
    device = "/dev/disk/by-label/nixos";
    fsType = "ext4";
  };

  #fileSystems."/opt" = {
  #  device = "/dev/disk/by-uuid/a43a2ae7-6293-477f-af2c-3488bb2c9ebf";
  #  fsType = "ext4";
  #  options = [ "user" "users" "exec" ];
  #};

  # bind mounts
  # everything should be accessed through bind mounts where possible
  fileSystems."/mnt/data" = {
    device = "/tank";
    options = [ "bind" ];
  };

  fileSystems."/export/data" = {
    device = "/mnt/data";
    options = [ "bind" ];
  };

  #swapDevices = [ { device = "/opt/swapfile"; size=32768; } ];
}
