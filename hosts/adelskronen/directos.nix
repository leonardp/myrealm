{ config, shared, ... }:
{
  imports = [
    shared.modules.directos
  ];

  services.directos = {
    enable = true;
    #directosSecretFile = "${toString config.sops.secrets."directos/secret".path}";
    #directosKeyFile = "${toString config.sops.secrets."directos/key".path}";
    #sshKeyFile = "${toString config.sops.secrets.automaton.path}";
    directosSecretFile = config.sops.secrets."directos/secret".path;
    directosKeyFile = config.sops.secrets."directos/key".path;
    sshKeyFile = config.sops.secrets.automaton.path;
    repoUrl = "git@gitlab.com:leonardp/myrealm.git";
  };

  sops = {
    defaultSopsFile = ../../secrets/directus.yaml;
    age.sshKeyPaths = [ "/root/.ssh/id_ed25519" ];

    secrets = {
      "directos/key" = {
        key = "directos/key";
        mode = "0400";
        owner = "directus";
        group = "directus";
      };

      "directos/secret" = {
        key = "directos/secret";
        mode = "0400";
        owner = "directus";
        group = "directus";
      };

      automaton = {
        sopsFile = ../../secrets/ssh.yaml;
        key = "automaton";
        mode = "0400";
        owner = "directos";
        group = "directos";
      };
    };
  };

  users.users.directus.extraGroups = [ config.users.groups.keys.name ];
  systemd.services."directus-bootstrap-directos".after = [ "run-secrets.d.mount" ];

  users.users.directos.extraGroups = [ config.users.groups.keys.name ];
}
