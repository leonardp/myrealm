{ config, ... }:
{
  networking.hostName = "adelskronen";
  nixpkgs.hostPlatform = "x86_64-linux";
  time.timeZone = "Europe/Berlin";
}
