{
  description = "flake for packages/overlays etc.";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    deploy-rs.url = "github:serokell/deploy-rs";
    directus.url = "gitlab:lone4/directus-flake-parts";
    nixlos.url = "gitlab:lone4/nixlos";
    directos.url = "gitlab:lone4/dos-dev";
  };

  outputs = inputs@{ flake-parts, ... }:
    flake-parts.lib.mkFlake { inherit inputs; }
  {
      systems = [ "x86_64-linux" "aarch64-linux" ];

      imports = [
        inputs.flake-parts.flakeModules.easyOverlay
      ];

      perSystem = { pkgs, system, config, ... }: {
        overlayAttrs = {
          inherit (config.packages)
            deploy-rs
            directus
            nixlosSchema
            nixlosMigrations
            directosSchema
            directosMigrations
          ;
        };

        packages = {
          deploy-rs = inputs.deploy-rs.packages.${system}.default;
          directus = inputs.directus.packages.${system}.default;
          nixlosSchema = inputs.nixlos.packages.${system}.schema;
          nixlosMigrations = inputs.nixlos.packages.${system}.migrations;
          directosSchema = inputs.directos.packages.${system}.schema;
          directosMigrations = inputs.directos.packages.${system}.migrations;
        };

      };
      flake.overlays.python = import ./python;
      flake.overlays.localPackages = import ./packages-overlay.nix;
  };
}
