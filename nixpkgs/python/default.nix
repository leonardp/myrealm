final: prev:
let
  # python libraries and patches
  pyOverlay = {
    packageOverrides = py-final: py-prev: {
      base45 = py-final.callPackage ./base45.nix {};
      dearpygui = py-final.callPackage ./dearpygui.nix {};
    };
  };
in
# rec set=
rec {

  # ??
  #python3 = prev.python3.override pyOverlay { self = python3};
  python3 = prev.python3.override pyOverlay;

  # python applications are toplevel
  # pyApp = prev.callPackage ./pythonApp.nix { };

}
