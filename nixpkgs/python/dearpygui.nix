{ buildPythonPackage
, pkg-config
, cmake
, libX11
, libXrandr
, libXinerama
, libXcursor
, libXi
#, libXext
#, glfw
#, glew
}:
buildPythonPackage rec {
  pname = "dearpygui";
  version = "1.10.1";

  src = fetchGit {
    url = "https://github.com/hoffstadt/DearPyGui";
    rev = "eb979321b639757a8289c835528b91f012dbb803";
    submodules = true;
  };

  nativeBuildInputs = [
    pkg-config
    cmake
  ];

  buildInputs = [
    libX11.dev
    libXrandr.dev
    libXinerama.dev
    libXcursor.dev
    libXi.dev
    #libXext
    #glfw
    #glew
  ];

  #propagatedBuildInputs = [ cython ];

  #doCheck = true;
  dontUseSetuptoolsCheck = true;

  pythonImportsCheck = [
    "dearpygui"
  ];
}
