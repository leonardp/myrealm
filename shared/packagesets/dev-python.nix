{ pkgs, config, ... }:
{
  environment.systemPackages = with pkgs; [
    libsForQt5.full
    qtcreator
    (python3.withPackages(ps: with ps; [
      # this package is defined in ../ overlay test
      base45
      #dearpygui

      pyserial
      pyusb
      hidapi
      jinja2
      lxml
      future
      graphviz
      cryptography
      setuptools
      pyelftools
      pyparsing
      click
      requests
      twine
      wheel
      pyzmq
      numpy
      aiohttp

      pyside6
      pyqtgraph
      python-can
      cantools
    ]))
  ];
}
