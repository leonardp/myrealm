{ pkgs, config, ... }:
{
  environment.systemPackages = with pkgs; [
    firefox
    #ungoogled-chromium
    thunderbird
    libreoffice
    # too much!
    #texlive.combined.scheme-full
    imagemagick
    exiftool
    element-desktop
  ];
}
