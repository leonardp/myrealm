{ pkgs, config, ... }:
{
  environment.systemPackages = with pkgs; [
    # system/files
    htop
    fd
    silver-searcher
    pv
    file
    sysstat
    lsof
    tree

    # nix
    cachix

    # network
    wget
    curl
    socat
    iftop
    nmap
    rsync
    bmon
    mtr

    # useful cli tools
    graphviz
    mscgen
    dnsutils
    inetutils
    whois

    # compression
    xz
    lz4
    zip
    unzip
    unrar
    p7zip
    zstd

    # tools
    tmux
    git
    usbutils
    nfs-utils
    age
    ssh-to-age
    sops
    deploy-rs
    fzf
  ];

  environment.variables = {
    FZF_DEFAULT_OPTS = "--height=25% --layout=reverse --info=inline --border --padding=1";
  };

  programs.bash = {
    completion.enable = true;
    interactiveShellInit  =  ''
      HISTSIZE=-1
      HISTFILESIZE=-1
      HISTCONTROL=ignoreboth:erasedups
      shopt -s histappend
      PROMPT_COMMAND="history -n; history -w; history -c; history -r; $PROMPT_COMMAND"

      if command -v fzf-share >/dev/null; then
        source "$(fzf-share)/key-bindings.bash"
        source "$(fzf-share)/completion.bash"
      fi
    '';
  };
}
