{ pkgs, config, ... }:
{
  environment.systemPackages = with pkgs; [
    htop
    file
    lsof
    tmux
    tree

    wget
    curl
    rsync

    socat
    dnsutils
    inetutils
    whois
    nmap

    xz
    lz4
    zstd

    usbutils
    gitMinimal
  ];
}
