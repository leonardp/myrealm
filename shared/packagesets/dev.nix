{ pkgs, config, ... }:
{
  environment.systemPackages = with pkgs; [
    gitAndTools.gitFull
    difftastic
    sqlitebrowser
    entr
    parallel
    jq
  ];
}
