{ pkgs, config, ... }:
{
  environment.systemPackages = with pkgs; [
    firefox
    ungoogled-chromium
    thunderbird

    pam_krb5
    exiftool
    gparted
    virt-manager
    virt-viewer

    #texlive.combined.scheme-full
    libreoffice
    pandoc

    inkscape
    imagemagick
    blender
    obs-studio
    ffmpeg-full

    element-desktop
    stellarium
    fstl
    meshlab
  ];
}
