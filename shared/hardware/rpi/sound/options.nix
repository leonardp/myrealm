{ pkgs, ... }:
{
  a2dpsink.enable = false;

  # Auto Reconnect
  hardware.bluetooth = {
    disabledPlugins = [ "sap" "vcp" "mcp" "bap" ];
    settings = {
      Gerneral = {
        JustWorksRepairing = "always";
        Experimental = "true";
      };
    };
  };

  security.rtkit.enable = true;

  services.pipewire = {
    enable = true;
    wireplumber.enable = true;
    pulse.enable = true;
    alsa = {
      enable = true;
      #support32Bit = true;
    };
  };
}
