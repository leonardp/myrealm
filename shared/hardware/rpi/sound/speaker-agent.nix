{ stdenv, lib }:
stdenv.mkDerivation {
  pname = "speaker-agent";
  version = "1.0";

  #propagatedBuildInputs = [ dbus-python ];

  src = ./.;

  dontUnpack = true;
  installPhase = "install -Dm755 ${./speaker-agent.py} $out/speaker-agent.py";
}

