{ config, pkgs, lib, ... }:
{
  nixpkgs.overlays = [(final: super: {
    zfs = super.zfs.overrideAttrs(_: {
      meta.platforms = [];
    });
  })];
  #boot.zfs.enabled = lib.mkDefault false;

  hardware.enableRedistributableFirmware = true;
  networking.wireless.enable = true;

  nixpkgs.hostPlatform = "aarch64-linux";

  boot.loader.grub.enable = false;
  boot.loader.generic-extlinux-compatible.enable = true;
  #boot.supportedFilesystems = ["fatfs""ext4"];

  boot.kernelPackages = pkgs.linuxPackages_rpi3;
  #boot.kernelPackages = pkgs.linuxKernel.kernels.linux_rpi3;
  boot.kernelParams = lib.mkDefault [
    "cma=320M"
    "console=ttyS1,115200n8"
    "console=tty0"
  ];
  boot.initrd.kernelModules = [ "vc4" "bcm2835_dma" "i2c_bcm2835" "ahci"];

  # https://wiki.nixos.org/wiki/NixOS_on_ARM/Raspberry_Pi_3
  # The bluetooth controller is by default connected to the UART device at /dev/ttyAMA0 and needs to be enabled through btattach
  systemd.services.btattach = {
    before = [ "bluetooth.service" ];
    after = [ "dev-ttyAMA0.device" ];
    wantedBy = [ "multi-user.target" ];
    serviceConfig = {
      ExecStart = "${pkgs.bluez}/bin/btattach -B /dev/ttyAMA0 -P bcm -S 3000000";
    };
  };

}
