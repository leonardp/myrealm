{ config, ... }:
{
  #boot.kernelPackages = pkgs.linuxPackagesNanopiR4S;
  boot.consoleLogLevel = lib.mkDefault 7;
  boot.kernelParams = ["cma=32M" "console=ttyS2,115200n8" "console=tty0"];

  boot.loader = {
    grub.enable = false;
    generic-extlinux-compatible.enable = true;
  };

  hardware.enableRedistributableFirmware = true;
}
