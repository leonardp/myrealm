{ config, pkgs, lib, nixos-hardware, ... }:
{
  imports = [
    nixos-hardware.nixosModules.pine64-pinebook-pro
  ];

  console = { keyMap = "uk"; };

  services.libinput.enable = true;
  services.xserver = {
    enable = true;
    videoDrivers = [ "panfrost" ];
    xkb.layout = "gb";
  };

  boot.loader.grub.enable = false;
  boot.loader.generic-extlinux-compatible.enable = true;
  boot.consoleLogLevel = lib.mkDefault 7;

  fileSystems = {
    "/" = {
      #device = "/dev/disk/by-path/platform-fe330000.sdhci-part1";
      device = "/dev/mmcblk2p1";
      fsType = "ext4";
    };
    "/data" = {
      device = "/dev/nvme0n1p1";
      fsType = "ext4";
      options = [ "user" "users" "exec" ]; # TODO ok?
    };
  };

  swapDevices = [ { device = "/data/swapfile"; size=8192; } ];
}
