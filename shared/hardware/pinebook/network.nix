{
  networking.wireless.enable = false;

  networking.useNetworkd = true; # experimental
  networking.dhcpcd.allowInterfaces = ["wlan0"];

  networking.firewall.enable = false;
}
