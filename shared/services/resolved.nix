{ pkgs, lib, config, ... }:
let
  cloudflare = [
    "1.1.1.1"
  ];
  google = [
    "8.8.8.8"
    "4.4.4.4"
  ];
in
{
  services.resolved = {
    enable = true;
    domains = [ "local" ];

    fallbackDns = cloudflare ++ google ;

    # workaround for https://github.com/NixOS/nixpkgs/issues/66451
    # dnssec = "false";

    #llmnr = "false";
    #extraConfig = ''
    #  # might help if there is a local nameserver instance
    #  DNSStubListener=no
    #  #DNSOverTLS=opportunistic # *good* for captive portals
    #  Cache=no
    #  MulticastDNS=no # conflicts with avahi
    #  #ReadEtcHosts=yes
    #'';
  };
}
