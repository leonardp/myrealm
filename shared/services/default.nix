{ ... }:
{
  imports = [
    ./ntp.nix
    ./resolved.nix
    ./ssh.nix
  ];
}
