{ pkgs, lib, ... }:
{
  # xwayland
  #programs.xwayland.enable = true;

  services.displayManager = {
    defaultSession = "gnome";
  };

  services.xserver = {
    enable = true; # enable X
    displayManager = {
      gdm.enable = true;
      #gdm.wayland = true;
      gdm.wayland = false;
    };
    desktopManager = {
      xterm.enable = false; # disable xterm session
      gnome = {
        enable = true;
        extraGSettingsOverrides = ''
          [org.gnome.desktop.peripherals.mouse]
          middle-click-emulation=true
        '';
      };
    };
  };

  ## userspace vfs
  services.gvfs.enable = true;

  # disable what we can?
  # https://nixos.wiki/wiki/GNOME#Excluding_some_GNOME_applications_from_the_default_install
  # environment.gnome.excludePackages = [ ];

  services.gnome = {
    sushi.enable = true;
    rygel.enable = true;
  };

  environment.systemPackages = with pkgs; [
    pkgs.gnome-themes-extra
    gnomeExtensions.gsconnect
    kitty
  ];

  # udev rules for android -> factor out
  #services.udev.packages = [ pkgs.android-udev-rules ];
  services.udev.packages = with pkgs; [ gnome-settings-daemon ];
  services.udev.extraRules = ''
    SUBSYSTEM=="usb", ATTR{idVendor}=="0a9d", ATTR{idProduct}=="ff40", MODE="0660", GROUP="uucp", ENV{ID_MTP_DEVICE}="1", SYMLINK+="libmtp"
  '';
}
