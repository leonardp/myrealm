{ config, ...}:
{
  security.sudo = {
    wheelNeedsPassword = false;
    execWheelOnly = true;
  };
}
