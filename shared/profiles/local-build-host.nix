{ config, ... }:
{
  # use adelskronen as binary cache and distributed builder
  nix.binaryCaches = [ "http://adelskronen.local" ];
  nix.binaryCachePublicKeys = [ "${pubkey.leo_bin_cache}" ];
  nix.distributedBuilds = true;
  nix.buildMachines = [ {
    hostName = "adelskronen.local";
    systems = [ "x86_64-linux" "aarch64-linux"];
    sshUser = "root";
    sshKey = "/root/.ssh/id_ed25519";
    maxJobs = 16;
    supportedFeatures = [ "kvm" "big-parallel" ];
  }];
}
