{ ... }:
{
  imports = [
    ./baseline.nix
    ./nvim.nix
  ];
}
