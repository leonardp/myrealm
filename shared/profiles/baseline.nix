{ config, pkgs, lib, ... }:
{
  nixpkgs.config = {
    # Allow proprietary packages
    allowUnfree = true;
    #allowBroken = true;
  };

  # unstable nix command with flakes
  nix = {
    #package = pkgs.nixVersions.nix_2_20;
    package = pkgs.nixVersions.latest;
    settings = {
      stalled-download-timeout = 1000;
      experimental-features = "nix-command flakes";
    };
  };

  # mount tmpfs on /tmp
  # -> not good for build hosts and machines with little RAM
  # -> mkForce false
  boot.tmp.useTmpfs = lib.mkDefault true;

  # Select internationalisation properties.
  console = {
    font = "Lat2-Terminus16";
  };
  i18n = {
    defaultLocale = "en_US.UTF-8";
    extraLocaleSettings = {
      LC_ALL = "en_US.UTF-8";
    };
  };

  system.stateVersion = "23.05"; # Did you read the comment?
}
