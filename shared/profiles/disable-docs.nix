{ config, ... }:
{
  documentation.enable = false;
  documentation.doc.enable = false;
  documentation.dev.enable = false;
  documentation.man.enable = false;
  documentation.info.enable = false;
  documentation.nixos.enable = false;
}
