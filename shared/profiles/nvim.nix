{ config, pkgs, ... }:
{
  programs.neovim = {
    enable = true;
    defaultEditor = true;
    viAlias = true;
    vimAlias = true;
    withPython3 = true;
    withNodeJs = true;

    configure = {
        packages.myPlugins = with pkgs.vimPlugins; {
          start = [
            vim-lastplace
            vim-nix
            jedi-vim
            nerdtree
            vim-airline
            vim-yaml
          ];
          opt = [];
        };
        customRC = ''
          " 80 col width marker
          highlight ColorColumn ctermbg=Black ctermfg=DarkRed
          call matchadd('ColorColumn', '\%81v', 100)

          " Highlight trailing spaces
          " http://vim.wikia.com/wiki/Highlight_unwanted_spaces
          highlight ExtraWhitespace ctermbg=red guibg=red
          match ExtraWhitespace /\s\+$/
          autocmd BufWinEnter * match ExtraWhitespace /\s\+$/
          autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
          autocmd InsertLeave * match ExtraWhitespace /\s\+$/
          autocmd BufWinLeave * call clearmatches()

          set foldmethod=indent
          " set foldnestmax=10
          set foldlevel=0
          " set foldenable

          " open NERDTree on the right side
          let g:NERDTreeWinPos="right"

          filetype plugin indent on
          set mouse=
          set clipboard+=unnamedplus
          set number

          " Tabs are 8 characters, and thus indentations are also 8 characters.
          " There are heretic movements that try to make indentations 4 (or even 2!) characters deep,
          " and that is akin to trying to define the value of PI to be 3.
          set tabstop=8
          set softtabstop=8
          set shiftwidth=8
          set noexpandtab

          function! Heretic()
            setlocal tabstop=2
            setlocal softtabstop=2
            setlocal shiftwidth=2
            setlocal expandtab
          endfunction

          autocmd FileType php,json,javascript,typescript,vue,css call Heretic()
        '';
    };

  };
}
