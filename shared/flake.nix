{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";

    haumea = {
      url = "github:nix-community/haumea/v0.2.2";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = inputs@{ self, haumea, nixpkgs, ... }: {
    hardware = haumea.lib.load { src = ./hardware;
      #inputs = { pkgs = inputs.nixpkgs; inherit (nixpkgs) lib;};
      loader = haumea.lib.loaders.verbatim;
    };

    profiles = haumea.lib.load { src = ./profiles;
      loader = haumea.lib.loaders.verbatim;
    };

    packagesets = haumea.lib.load { src = ./packagesets;
      loader = haumea.lib.loaders.verbatim;
    };

    services = haumea.lib.load { src = ./services;
      loader = haumea.lib.loaders.verbatim;
    };

    storage = haumea.lib.load { src = ./storage;
      loader = haumea.lib.loaders.verbatim;
    };

    modules = haumea.lib.load { src = ./modules;
      loader = haumea.lib.loaders.verbatim;
    };
  };
}
