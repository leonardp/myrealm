{ config, ... }:
{
  fileSystems."/mnt/nfsshare" = { device = "192.168.0.10:/"; fsType = "nfs"; options=[ "x-systemd.idle-timeout=600" "x-systemd.automount" "_netdev" ]; };
}
