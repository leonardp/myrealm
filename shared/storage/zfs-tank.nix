{ config, ... }:
{
  # required for ZFS -> `head -c 8 /etc/machine-id`
  networking.hostId = "307e7c9a";

  #boot.zfs.enableUnstable = true;
  #boot.kernelPackages = config.boot.zfs.package.latestCompatibleLinuxPackages;
  boot.supportedFilesystems = [ "zfs" ];
  boot.kernelParams = [
    "zfs.zfs_arc_max=8589934592"
  ];

  fileSystems."/tank" = {
    device = "tank/data";
    fsType = "zfs";
    options = [ "user" "users" ];
  };

  fileSystems."/mnt/data" = {
    device = "/tank";
    options = [ "bind" ];
  };
}
