{ config, pkgs, lib, ... }:
let
  cfg = config.services.directos;

  write = pkgs.writeShellScript "write" ''
    git pull

    echo $1 > payload.json

    host=$(jq -r '.name' payload.json)
    mkdir -p $host
    rm -f $host/directus-*

    cat << EOF > $host/directus-baseline.nix
    { config, ... }:
    {
      networking.hostName = "$(jq -r .baseline.hostname payload.json)";
      nixpkgs.hostPlatform = "$(jq -r .baseline.platform payload.json)";
      time.timeZone = "$(jq -r .baseline.timezone payload.json)";
    }
    EOF

    imports=$(jq -r '.imports[]' payload.json)

    cat << EOF > $host/directus-imports.nix
    { shared, ... }:
    {
      imports = [
        $imports
      ];
    }
    EOF

    names=($(jq -r '.configs[].name' payload.json))

    for k in ''${!names[@]}; do
    name=''${names[$k]}
    cat << EOF > $host/directus-$name.nix
    $(jq -r .configs[$k].config payload.json)
    EOF
    done

    users=($(jq -cr '.users[].name' payload.json))

    cat << EOF > $host/directus-users.nix
    { config, ... }:
    {
      users.users = {
    EOF

    for k in ''${!users[@]}; do
    name=''${users[$k]}
    keys=$(jq  ".users[$k].authorized_keys[].key | select (.!=null)"  payload.json)
    groups=$(jq -r ".users[$k].groups | select (.!=null) | @sh" payload.json | tr "'" "\"")

    if [ $name = "root" ]; then
    cat << EOF >> $host/directus-users.nix
        root = {
          openssh.authorizedKeys.keys = [
            $keys
          ];
        };
    EOF
    else
    cat << EOF >> $host/directus-users.nix
        $name = {
          isNormalUser = true;
          openssh.authorizedKeys.keys = [
            $keys
          ];
          extraGroups = [ $groups ];
        };
    EOF
    fi
    done

    cat << EOF >> $host/directus-users.nix
      };
    }
    EOF

    rm payload.json
  '';

  check = pkgs.writeShellScript "check" ''
   git add .
   nix flake check
  '';

  deploy = pkgs.writeShellScript "deploy" ''
   deploy .#$1 --hostname $2
  '';

  gitpush = pkgs.writeShellScript "gitpush" ''
    git commit -m "directos automaton: $1"
    git push
  '';

  echojq = pkgs.writeShellScript "echojq" ''
   jq . $1
  '';
in
{
  options.services.directos = {
    enable = lib.mkEnableOption (lib.mdDoc "directos");

    directosPort = lib.mkOption {
      type = lib.types.port;
      default = 7777;
      description = lib.mdDoc ''
        TCP port number for directos.
      '';
    };

    directosWebhookPort = lib.mkOption {
      type = lib.types.port;
      default = 9000;
      description = lib.mdDoc ''
        TCP port number for webhook service.
      '';
    };

    directosKeyFile = lib.mkOption {
      type = with lib.types; nullOr path;
      default = null;
      description = lib.mdDoc ''
        path to directos key file.
      '';
    };

    directosSecretFile = lib.mkOption {
      type = with lib.types; nullOr path;
      default = null;
      description = lib.mdDoc ''
        path to directos secret file.
      '';
    };

    sshKeyFile = lib.mkOption {
      type = with lib.types; nullOr path;
      default = null;
      description = lib.mdDoc ''
        Path to SSH private key used for deployments and/or git.
      '';
    };

    gitConfig = lib.mkOption {
      type = with lib.types; nullOr str;
      default = ''
        [user]
	name = DirectOS Automaton
	email = automaton@living-systems.dev
      '';
      description = lib.mdDoc ''
        .gitconfig of your DirectOS 'automaton'.
      '';
    };

    repoUrl = lib.mkOption {
      type = with lib.types; nullOr str;
      default = "https://github.com/leonardp/directos";
      description = lib.mdDoc ''
        URL to the github repo containing 'DirectOS'.
      '';
    };
  };

  config = lib.mkIf cfg.enable {
    networking.hosts = {
      "127.0.0.1" = [ "directos.local" "directos-ttyd.local" "directos-webhook.local" ];
    };

    nix.settings = {
      trusted-users =  [ "directos" ];
    };

    users = {
      users.directos = {
        group = "directos";
        #hashedPassword = "$y$j9T$hNyygTpp00smvZOJk/VLZ.$wSzRTzR5imEWl/qT2tJ2JVAoiJxptRtGLSCoha6Tkf0";
        extraGroups = [ "keys" ];
        isSystemUser = true;
        home = "/var/lib/directos";
        createHome = true;
        useDefaultShell = true;
      };
      groups.directos = { };
    };

    services.directus.instances = {
      directos = {
        enable = true;
        url = "http://directos.local";
        address = "127.0.0.1";
        port = cfg.directosPort;
        secretFile = cfg.directosSecretFile;
        keyFile = cfg.directosKeyFile;

        env = {
          "DB_CLIENT" = "sqlite3";
          "DB_FILENAME" = "./data.db";
          "ADMIN_EMAIL" = "admin@example.com";
          "ADMIN_PASSWORD" = "admin";
          "IMPORT_IP_DENY_LIST" = "169.254.169.254";
          "EXTENSIONS_PATH" = "/var/lib/directus/directos/extensions";
          "CONTENT_SECURITY_POLICY_DIRECTIVES__FRAME_SRC" = "'self',http://directos-ttyd.local";
        };
        bootstrap = {
          schema = pkgs.directosSchema;
          migrations = pkgs.directosMigrations;
        };
      };
    };

    systemd.services.ttyd = {
      description = "ttyd Web Server Daemon";
      wantedBy = [ "multi-user.target" ];
      serviceConfig = {
        User = "directos";
        Group = "directos";
      };

      script = ''
        ${pkgs.ttyd}/bin/ttyd \
        --port 7861 \
        --signal 1 \
        --writable \
        --client-option enableSixel=true \
        --terminal-type xterm-256color \
        --max-clients 3 \
        --cwd $HOME \
        /run/current-system/sw/bin/bash
      '';
    };

    systemd.services.webhook.path = with pkgs; [
      git
      gnutar
      gzip
      nix
      deploy-rs
      openssh
      jq
    ];

    services.webhook = {
      enable = true;
      user = "directos";
      group = "directos";
      ip = "127.0.0.1";
      port = cfg.directosWebhookPort;

      hooks = {
        write = {
          command-working-directory = "/var/lib/directos/directos/hosts";
          execute-command = "${write}";
          include-command-output-in-response = true;
          pass-arguments-to-command = [{
            source = "entire-payload";
          }];
        };

        check = {
          command-working-directory = "/var/lib/directos/directos";
          execute-command = "${check}";
          include-command-output-in-response = true;
        };

        deploy = {
          command-working-directory = "/var/lib/directos/directos";
          execute-command = "${deploy}";
          include-command-output-in-response = true;
          pass-arguments-to-command = [
            { source = "payload";
              name = "name"; }
            { source = "payload";
              name = "deployment_ip"; }
          ];
        };

        gitpush = {
          command-working-directory = "/var/lib/directos/directos";
          execute-command = "${gitpush}";
          include-command-output-in-response = true;
          pass-arguments-to-command = [
            { source = "payload";
              name = "name"; }
          ];
        };

        echojq = {
          command-working-directory = "/var/lib/directos/directos";
          execute-command = "${echojq}";
          pass-arguments-to-command = [{
            source = "entire-payload";
          }];
        };
      };
    };

    services.nginx = {
      enable = true;
      recommendedTlsSettings = true;
      recommendedGzipSettings = true;
      recommendedOptimisation = true;
      recommendedProxySettings = true;

      virtualHosts = {
        "directos.local" = {
          listen = [ { addr = "127.0.0.1"; port = 80; } ];
          locations."/" = {
            proxyPass = "http://127.0.0.1:${toString cfg.directosPort}";
            proxyWebsockets = true;
          };
        };

        "directos-webhook.local" = {
          listen = [ { addr = "127.0.0.1"; port = 80; } ];
          locations."/" = {
            proxyPass = "http://127.0.0.1:${toString cfg.directosWebhookPort}";
            proxyWebsockets = true;
          };
        };

        "directos-ttyd.local" = {
          listen = [ { addr = "127.0.0.1"; port = 80; } ];
          locations."/" = {
            proxyPass = "http://127.0.0.1:7861";
            proxyWebsockets = true;
          };
        };
      };
    };

    systemd.services.directos-user-bootstrap = {
      wantedBy = [ "multi-user.target" ];
      requires = [ "network-online.target" ];
      restartIfChanged = false;
      path = with pkgs; [ git gnutar gzip openssh ];
      serviceConfig = {
        Type = "oneshot";
        User = "directos";
        Group = "directos";
        WorkingDirectory="/var/lib/directos";
      };

      script = ''
        if !(test -e ".bootstrapped"); then
        ${lib.optionalString (cfg.sshKeyFile != null) ''
        mkdir -p .ssh
        chmod -R 700 .ssh/
        cp ${cfg.sshKeyFile} .ssh/id_ed25519
        chmod 600 .ssh/id_ed25519
        ''}

        ${lib.optionalString (cfg.gitConfig != null) ''
        echo "${cfg.gitConfig}" > .gitconfig
        ''}

        ${lib.optionalString (cfg.repoUrl != null) ''
        git clone ${cfg.repoUrl} directos
        ''}
        fi
        touch .bootstrapped
      '';
    };
  };
}
